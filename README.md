# Mystery Corp Frontend

This is the Mystery Corp todo app frontend. It is a React/Redux single page app, deployed as a static site.
The structure of this codebase is based on the structure of other frontend code at Mystery Corp.

This app provides some boilerplate which you can use for your coding test, if you want.
Feel free to delete the todo stuff and add whatever you want.

## Project Structure

```
├── dist                    build artifacts (not in version control)
├── public                  static files to be deployed (in version control)
├── scripts                 bash scripts
└── src
    ├── api                 interface for all API calls
    ├── components          "dumb" presentational React components
    ├── constants            constants
    ├── containers          "smart" React components
    ├── types               FlowJS type definitions
    ├── state               Redux stuff (state / actions / reducers)
    ├── styles              all SCSS stylesheets
    ├── utilities           "helpful", "reusable" tools
    ├── views               view React components
    ├── app.js              root React component
    ├── index.js            app entry point
    └── index.hbs           Handlebars HTML template
```

## System Setup

You will need Node >=8 and [yarn](https://yarnpkg.com/en/) installed locally.

```bash
yarn install
```

### Type Checking (OPTIONAL)

This project use FlowJS for type checking.
See more details in `.flowconfig`. 

You can install "Flow Language Support" in VSCode to view type annotations.

If you are not comfortable using FlowJS, ignore it.

### Linting

Linting is done with [Prettier](https://prettier.io/).

```bash
yarn format  # Format JS code (recommended)
yarn lint  # Lint JS code (run in CI jobs)
```

## Development

Run the development server and webpack compilation:

```bash
rm -rf dist
yarn html  # Build HTML from Handlebars template
yarn serve  # Run dev server
```
## Deployment

To simulate a build you can run:

```bash
rm -rf dist
yarn install
yarn build-release
```

All build artifacts will appear in `dist/`