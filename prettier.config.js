// Prettier (linter) configuration
module.exports = {
  endOfLine: 'lf',
  trailingComma: 'es5',
  tabWidth: 2,
  parser: 'flow',
  semi: false,
  singleQuote: true,
  printWidth: 90,
}
