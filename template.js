/*

  Converts src/index.hbs into dist/index,html
  and injects build variables into the template.

*/
const path = require('path')
const fs = require('fs')
const Handlebars = require('handlebars')

// Read environment variables
let ENV
const DEV_ENVARS_PATH = './env.dev'
if (!process.env.BUILD_RELEASE) {
  ENV = fs
    .readFileSync(DEV_ENVARS_PATH, 'utf8')
    .split('\n')
    .filter(envar => !envar.startsWith('#'))
    .map(envar => envar.split('='))
    .filter(([key, value]) => key)
    .reduce((obj, [key, value]) => {
      obj[key] = value
      return obj
    }, {})
  console.log('Development envars: ', ENV)
} else {
  ENV = process.env
}

if (!ENV.STATIC_ROOT) {
  throw new Error('No static root defined')
}

// Read Handlebars template file, turn it into HTML, then write it
const onReadFile = (err, htmlText) => {
  if (err) {
    return console.error('Unable to read index.hbs')
  }
  const html = getHTML(htmlText)
  onWriteIndex(html)
}

// Turn Handlebars into HTML
const getHTML = htmlText => {
  const template = Handlebars.compile(htmlText)
  return template({
    staticRoot: ENV.STATIC_ROOT,
    jsBuildHash: ENV.JS_BUILDHASH,
    cssBuildHash: ENV.CSS_BUILDHASH,
  })
}

// Write HTML to file.
onWriteIndex = html => {
  fs.writeFile('dist/index.html', html, () => console.log('HTML written.'))
}

fs.readFile('src/index.hbs', 'utf8', onReadFile)
