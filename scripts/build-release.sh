#/bin/bash
set -e

# Release envars (ideally injected via CI/CD system)
export STATIC_ROOT=./static
export SERVER_URL=http://localhost:8000

echo -e "\nCleaning build output directory."
rm -rf dist
mkdir -p dist/static

echo -e "\nBuilding release JavaScript and CSS."
yarn release-webpack

echo -e "\nGenerating cache busting hashes."

export JS_BUILDHASH="$(cat dist/static/mystery-corp-prod.js | md5sum | cut -d' ' -f1 | head -c 8)"
[[ -z "$JS_BUILDHASH" ]] && {
    echo "JS_BUILDHASH is empty"
    exit 1
}
echo "Using JS build hash $JS_BUILDHASH"
mv dist/static/mystery-corp-prod.js dist/static/mystery-corp-${JS_BUILDHASH}.js

export CSS_BUILDHASH="$(cat dist/static/mystery-corp-prod.css | md5sum | cut -d' ' -f1 | head -c 8)"
[[ -z "$CSS_BUILDHASH" ]] && {
    echo "CSS_BUILDHASH is empty"
    exit 1
}
echo "Using CSS build hash $CSS_BUILDHASH"
mv dist/static/mystery-corp-prod.css dist/static/mystery-corp-${CSS_BUILDHASH}.css

echo -e "\nBuilding release HTML."
export BUILD_RELEASE='true'
yarn release-html

echo -e "\nCollecting static files."
cp -r public/static/** dist/static

# Print build output to the console.
echo -e "\nDone building release assets:"
find dist/

# Final santity check to ensure that we've built all the important stuff.
echo -e "\nChecking for build assets."
[[ ! -f dist/index.html ]] && {
    echo "No HTML file found"
    exit 1
}

[[ ! -f dist/static/mystery-corp-${JS_BUILDHASH}.js ]] && {
    echo "No JavaScript file found"
    exit 1
}

[[ ! -f dist/static/mystery-corp-${CSS_BUILDHASH}.css ]] && {
    echo "No CSS file found"
    exit 1
}

echo -e "\nBuild finished.\n"
