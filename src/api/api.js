// @flow
import uuid from 'uuid'
import type { Todo } from 'types'

const FAKE_API_DELAY = 200 // ms
let todos = [] // a pretend backend database.

// Returns an `api` object - which wraps all HTTP API calls.
// This implementation fakes HTTP requests to an imaginary backend server.
// Feel free to delete this and replace the methods with real HTTP requests.
export const api = {
  todo: {
    list: (): Promise<Array<Todo>> => {
      return new Promise(resolve => setTimeout(() => resolve(todos), FAKE_API_DELAY))
    },
    create: (text: string): Promise<Todo> => {
      const todo: Todo = { id: uuid(), text }
      todos = [...todos, todo]
      return new Promise(resolve => setTimeout(() => resolve(todo), FAKE_API_DELAY))
    },
    update: (id: string, text: string): Promise<Todo> => {
      todos = todos.map(t => (t.id === id ? { ...t, text } : t))
      const todo: Todo = { id, text }
      return new Promise(resolve => setTimeout(() => resolve(todo), FAKE_API_DELAY))
    },
    delete: (id: string): Promise<void> => {
      todos = todos.filter(t => t.id !== id)
      return new Promise(resolve => setTimeout(() => resolve(), FAKE_API_DELAY))
    },
  },
}
