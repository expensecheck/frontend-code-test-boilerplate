// @flow
import { init as todoInit } from './todo/init'

import type { Redux, Reducer, Action } from 'types'

// Initial Redux state for the app
export const init: Redux = {
  todo: todoInit,
}
