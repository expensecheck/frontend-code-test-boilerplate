// @flow
import type { Redux, Action, Reducer } from 'types'

export const pipe = (...fns: Array<Reducer>) => (state: Redux, action: Action) =>
  fns.reduce((s, f) => f(s, action), state)

type IdObject = { id: string }

export function updateIfMatch<T: IdObject>(item: T, updateFn: T => T): T => T {
  return (i: T) => (i.id === item.id ? updateFn(i) : i)
}

function replaceIfMatch<T: IdObject>(item: T): T => T {
  return (i: T) => (i.id === item.id ? item : i)
}

function itemInList<T: IdObject>(item: T, list: Array<T>): boolean {
  return list.some(i => i.id === item.id)
}

export function updateOrAppend<T: IdObject>(item: T, list: Array<T>): Array<T> {
  return itemInList<T>(item, list) ? list.map(replaceIfMatch<T>(item)) : [...list, item]
}
