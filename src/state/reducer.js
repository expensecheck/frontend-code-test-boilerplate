// @flow
import { reducer as todo } from './todo/reducer'

import { init } from './init'
import { pipe } from './utils'
import type { Reducer } from 'types'

// The final reducer function, which we pass to Redux.
export const reducer: Reducer = (state, action) => pipe(todo)(state || init, action)
