// @flow
import { updateIfMatch, updateOrAppend } from 'state/utils'

import type { Reducer, Action, Redux, Todo } from 'types'

import { init } from 'state/init'

// Todo operations
export const reducer: Reducer = (state, action) => {
  if (!state) return init
  switch (action.type) {
    // Request todos.
    case 'LOAD_TODO':
      return {
        ...state,
        todo: {
          ...state.todo,
          loading: true,
        },
      }

    // Receive a list of todos.
    case 'LIST_TODOS':
      return {
        ...state,
        todo: {
          ...state.todo,
          loading: false,
          items: action.todos,
        },
      }

    // Add or update a todo
    case 'UPSERT_TODO':
      return {
        ...state,
        todo: {
          ...state.todo,
          loading: false,
          items: updateOrAppend<Todo>(action.todo, state.todo.items),
        },
      }

    // Delete a todo
    case 'DELETE_TODO':
      return {
        ...state,
        todo: {
          ...state.todo,
          loading: false,
          items: state.todo.items.filter(t => t.id !== action.todo.id),
        },
      }
  }
  return state
}
