// @flow
import { api } from 'api'
import type { Todo, Action, Dispatch } from 'types'

// Todo actions
export const actions = {
  // Get a list of todos from the backend.
  list: () => (dispatch: Dispatch): Promise<Array<Todo>> => {
    dispatch({ type: 'LOAD_TODO' })
    return api.todo.list().then(todos => {
      dispatch({ type: 'LIST_TODOS', todos })
      return todos
    })
  },
  // Create a new todo in the backend.
  create: (text: string) => (dispatch: Dispatch): Promise<Todo> => {
    dispatch({ type: 'LOAD_TODO' })
    return api.todo.create(text).then(todo => {
      dispatch({ type: 'UPSERT_TODO', todo })
      return todo
    })
  },
  // Update a todo
  update: (todo: Todo, text: string) => (dispatch: Dispatch): Promise<Todo> => {
    dispatch({ type: 'LOAD_TODO' })
    return api.todo.update(todo.id, text).then(todo => {
      dispatch({ type: 'UPSERT_TODO', todo })
      return todo
    })
  },
  // Delete a todo.
  delete: (todo: Todo) => (dispatch: Dispatch): Promise<void> => {
    dispatch({ type: 'LOAD_TODO' })
    return api.todo.delete(todo.id).then(() => {
      dispatch({ type: 'DELETE_TODO', todo })
    })
  },
}
