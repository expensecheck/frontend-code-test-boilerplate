// @flow
import { api } from 'api'
import type { TodoState } from 'types'

// Todo initial state.
export const init = {
  items: [],
  loading: true,
}
