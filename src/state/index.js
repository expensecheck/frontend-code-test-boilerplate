// @flow
import { applyMiddleware, createStore } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'

import { reducer } from './reducer'
import { actions } from './actions'
import { init } from './init'
import type { Redux, Action, Dispatch } from 'types'

const loggerMiddleware = createLogger()
const middleware = applyMiddleware<Redux, Action, Dispatch>(
  thunkMiddleware,
  loggerMiddleware
)
const store = createStore<Redux, Action, Dispatch>(reducer, init, middleware)
export { store, actions }
