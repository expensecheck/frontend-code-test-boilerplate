// @flow
import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { Provider } from 'react-redux'
import * as Views from 'views'

import { store } from 'state'

export const App = () => (
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route path="/about" render={Views.AboutView} />
        <Route path="/" render={Views.HomeView} />
      </Switch>
    </BrowserRouter>
  </Provider>
)
