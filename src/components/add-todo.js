// @flow
import React, { useState } from 'react'
import styled from 'styled-components'

type Props = {
  onAdd: string => any,
}

export const AddTodoForm = ({ onAdd }: Props) => {
  const [text, setText] = useState('')
  const onClick = () => {
    if (text) onAdd(text)
  }
  return (
    <WrapperEl>
      <InputEl type="text" value={text} onChange={e => setText(e.target.value)} />
      <button onClick={onClick}>Add</button>
    </WrapperEl>
  )
}

const InputEl = styled.input`
  padding: 0.2rem;
`

const WrapperEl = styled.div`
  margin: 1rem 0;
`
