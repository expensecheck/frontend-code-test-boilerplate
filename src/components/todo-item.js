// @flow
import React, { useState } from 'react'
import styled from 'styled-components'

import type { Todo } from 'types'

type Props = {
  todo: Todo,
  onDelete: Todo => any,
}

export const TodoItem = ({ todo, onDelete }: Props) => (
  <TodoEl>
    <h1>{todo.text}</h1>
    <button onClick={() => onDelete(todo)}>Delete</button>
  </TodoEl>
)

const TodoEl = styled.div`
  margin: 1rem 0;
  background: #fff;
  padding: 0.5rem;
  border-radius: 5px;
`
