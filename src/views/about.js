// @flow
import React from 'react'
import { Link } from 'react-router-dom'

export const AboutView = () => (
  <div>
    <h1>About</h1>
    <p>This is a to do app where you can add, update or delete todos.</p>
    <Link to="/">Back to home</Link>
  </div>
)
