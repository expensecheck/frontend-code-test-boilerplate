// @flow
import React from 'react'
import { Link } from 'react-router-dom'
import { TodoContainer } from 'containers'

export const HomeView = () => (
  <div>
    <h1>Home</h1>
    <p>This is a to do app.</p>
    <Link to="/about">More about this app</Link>
    <TodoContainer />
  </div>
)
