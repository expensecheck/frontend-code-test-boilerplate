// @flow
import React from 'react'
import ReactDOM from 'react-dom'

import { App } from './app'

// @noflow
import styles from 'styles/index.scss'

// Mount React application.
const mount = document.getElementById('app')
if (mount) {
  ReactDOM.render(<App />, mount)
}
