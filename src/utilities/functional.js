// @flow
// Helper functions for map and reduce operations

// Flatten an array
export function flattenArray<T>(arr: Array<T>, sub: Array<T>): Array<T> {
  return [...arr, ...sub]
}

// Sort array in descending order
export const sortDesc = (a: any, b: any): number => (a > b ? -1 : 1)

// Sort array in ascending order
export const sortAsc = (a: any, b: any): number => (a > b ? 1 : -1)

// Ensure primitive item in array is unique
export const unique = (val: any, idx: number, self: Array<any>) =>
  self.indexOf(val) === idx

// All hail our brave new type system - Object.entries doesn't play nice with Flow
// https://github.com/facebook/flow/issues/2174
export function entries<T>(obj: { [string]: T }): Array<[string, T]> {
  const keys: string[] = Object.keys(obj)
  return keys.map(key => [key, obj[key]])
}

export function values<T>(obj: { [string]: T }): Array<T> {
  const keys: string[] = Object.keys(obj)
  return keys.map(key => obj[key])
}

export function keys<T>(obj: { [T]: any }): Array<T> {
  const keys: T[] = Object.keys(obj)
  return keys
}

export const pipe = (...fns: Array<any>) => (arg: any) => fns.reduce((a, f) => f(a), arg)

// Split array into chunks of a particular size.
export function chunkify<T>(arr: Array<T>, size: number): Array<Array<T>> {
  if (size < 1) return [[]]
  const items = [...arr]
  const chunks = []
  while (items.length > 0) {
    chunks.push(items.splice(0, size))
  }
  return chunks
}
