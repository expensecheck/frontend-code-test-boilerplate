// @flow
import React, { useEffect } from 'react'
import { useSelector, useDispatch, shallowEqual } from 'react-redux'
import styled from 'styled-components'

import { actions } from 'state'
import { TodoItem, AddTodoForm } from 'components'
import type { Todo, Redux } from 'types'

export const TodoContainer = () => {
  const dispatch = useDispatch()
  const isLoading = useSelector<boolean, Redux>(s => s.todo.loading)
  const todos = useSelector<Array<Todo>, Redux>(s => s.todo.items, shallowEqual)
  useEffect(() => {
    dispatch(actions.todo.list())
  }, [])
  const addTodo = (text: string) => dispatch(actions.todo.create(text))
  const deleteTodo = (todo: Todo) => dispatch(actions.todo.delete(todo))
  if (isLoading) return <LoadingEl>Loading todos...</LoadingEl>
  return (
    <React.Fragment>
      {todos.length > 0 && (
        <TodoListEl>
          {todos.map(t => (
            <TodoItem key={t.id} todo={t} onDelete={deleteTodo} />
          ))}
        </TodoListEl>
      )}
      <AddTodoForm onAdd={addTodo} />
    </React.Fragment>
  )
}

const TodoListEl = styled.div`
  border-radius: 5px;
  background: #ccc;
  padding: 1rem;
  margin: 1rem 0;
`

const LoadingEl = styled.div`
  margin: 1rem 0;
`
