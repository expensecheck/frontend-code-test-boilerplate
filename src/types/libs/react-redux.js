// @flow
import * as React from 'react'
import type { Action, Redux, Dispatch, Store } from 'types/redux'

declare module 'react-redux' {
  declare export var shallowEqual: Function
  declare export function useDispatch(): Dispatch
  declare export function useSelector<T, State>((State) => T, Function): T
  declare export function Provider(props: {
    store: Store,
    children: React.Node,
  }): React$Element<any>
}
