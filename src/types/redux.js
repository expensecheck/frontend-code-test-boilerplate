// @flow
import type { Store as ReduxStore, Reducer as ReduxReducer, DispatchAPI } from 'redux'
import type { Todo } from './todo'

export type TodoState = {
  +items: Array<Todo>,
  +loading: boolean,
}

export type Redux = {
  +todo: TodoState,
}

export type Action =
  | { +type: 'LOAD_TODO' }
  | { +type: 'LIST_TODOS', todos: Array<Todo> }
  | { +type: 'UPSERT_TODO', todo: Todo }
  | { +type: 'DELETE_TODO', todo: Todo }

export type GetState = () => Redux
export type Thunk = (dispatch: Dispatch, getState: GetState) => void
export type Dispatch = DispatchAPI<Action | Thunk>
export type Store = ReduxStore<Redux, Action, Dispatch>
export type Reducer = ReduxReducer<Redux, Action>
