const path = require('path')
const webpack = require('webpack')
const TerserPlugin = require('terser-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const baseConfig = require('./webpack.base.js')

module.exports = {
  ...baseConfig,
  mode: 'production',
  output: {
    path: __dirname + '/dist/static',
    filename: 'mystery-corp-prod.js',
  },
  optimization: {
    minimizer: [new TerserPlugin()],
  },
  plugins: [
    new MiniCssExtractPlugin({ filename: 'mystery-corp-prod.css' }),
    new webpack.DefinePlugin({
      // Build system envars
      STATIC_ROOT: JSON.stringify(process.env.STATIC_ROOT),
      SERVER_URL: JSON.stringify(process.env.SERVER_URL),
    }),
  ],
}
