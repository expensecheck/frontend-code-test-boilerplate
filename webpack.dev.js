const path = require('path')
const webpack = require('webpack')
const fs = require('fs')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const baseConfig = require('./webpack.base.js')

// Read environment variables
const ENV = fs
  .readFileSync('./env.dev', 'utf8')
  .split('\n')
  .filter(envar => !envar.startsWith('#'))
  .map(envar => envar.split('='))
  .filter(([key, value]) => key)
  .reduce((obj, [key, value]) => {
    obj[key] = value
    return obj
  }, {})

console.log('Development envars: ', ENV)
module.exports = {
  ...baseConfig,
  mode: 'development',
  devtool: 'cheap-module-eval-source-map',
  output: {
    filename: 'mystery-corp-dev.js',
  },
  devServer: {
    host: 'localhost',
    publicPath: 'http://localhost:3000/static/',
    contentBase: [path.join(__dirname, 'public'), path.join(__dirname, 'dist')],
    historyApiFallback: true,
    compress: true,
    port: 3000,
  },
  optimization: {
    minimize: false,
  },
  plugins: [
    new MiniCssExtractPlugin({ filename: 'mystery-corp-dev.css' }),
    new webpack.DefinePlugin({
      // Build system envars
      STATIC_ROOT: JSON.stringify(ENV.STATIC_ROOT),
      SERVER_URL: JSON.stringify(ENV.SERVER_URL),
    }),
  ],
}
